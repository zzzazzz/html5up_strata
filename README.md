# Html5up Strata
Portage SPIP du template Strata de Html5Up
https://html5up.net/strata

Compatible Spip 4

## Description

Jeu de squelettes simple et *responsive* plutôt orienté photo et avec peu de rédactionnel.
Il peut servir pour présenter le travail d'un artisan, d'un photographe, etc

De part sa structure éditoriale simple, ce jeu fonctionne sur les __[pages uniques](https://contrib.spip.net/Pages-uniques)__ du plugin du même nom et n'utilise donc pas les rubriques et articles traditionnels de SPIP.

2 gabarits principaux :

* une page d'accueil (sommaire.html)
* une page article (voir **Compositions** plus bas pour les différentes mises en page)

Sur la page d'accueil les articles sont classés par **numéro titre** puis **date inverse**

Concernant les photos, le design prend en compte un titre et une description (légende) pour chaque document.

### Paramétrage dans SPIP

* Définir un slogan `Configuration > Identité du site`
* Utiliser un logo de site (idem)
* Par défaut SPIP est configuré en HTML4, vous pouvez changer ça dans
`Configuration > Fonctions avancées > Norme HTML à suivre : HTML5`

## Les plugins utilisés (obligatoire)

* zcore
* identite_extra
* compositions
* pages

## Les plugins utilisés (non-obligatoire)

* mailcrypt
* sociaux

### Plugin *zcore*
Rien de spécial, la déclaration des z_blocs est dans `html5up_strata_fonctions.php`.

### Plugin *compositions*
Il est possible de modifier l'aspect de votre article avec ces deux compositions :
* article-image (génère un portfolio)
* article-contact (génère une page de contact)


### Plugin *identite_extra*

N'oubliez pas de configurer les champs dans : `Configuration > Identité du site`

### Plugin *pages*

Le tri des pages se fait par numéro titre : *03. Contact* par exemple.
Ou bien par date si pas de numérotation (du plus récent au plus ancien)

### Plugin *sociaux*

N'oubliez pas de configurer les champs si vous installez ce plugin : `Squelettes > Liens sociaux`


### Plugin *mailcrypt*
Si vous souhaitez protéger (disons réduire) votre email du spam.
Par défaut le mail affiché est l'auteur numéro 1 (tout comme pour la page contact).

----

Todo :
* Page de config pour définir l'ID de l'auteur à contacter
