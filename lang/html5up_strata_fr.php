<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adapte'			=> 'Adapté par',

	// C
	'conception'		=> 'Conception',
	'contacter'			=> 'Contacter',
	'cp'				=> 'Code postal',
		
	// P
	'propulse'			=> 'Propulsé par SPIP',
	'portfolio_plus'	=> 'Porfolio complet',
);
